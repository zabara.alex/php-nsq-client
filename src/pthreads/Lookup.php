<?php
declare(strict_types=1);

namespace Libs\Nsq;

class Lookup extends \Thread
{
    private const LOOKUP_TIMEOUT = 5;
    private const LOOKUP_INTERVAL = 30;

    public $nsqdInstances = '';

    public $stopSignal = false;

    private $config = null;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function run() : void
    {
        $lastLookup = 0;
        while (!$this->stopSignal) {
            if (!$this->nsqdInstances || $lastLookup + self::LOOKUP_INTERVAL < time()) {
                $tmpNsqdInstances = [];
                foreach ($this->config as $nsqLookupConf) {
                    $gateway = 'tcp://' . $nsqLookupConf['host'] . ':' . $nsqLookupConf['port'];
                    $ctx = stream_context_create(['socket' => ['tcp_nodelay' => true]]);
                    $socket = stream_socket_client($gateway, $errno, $errstr, self::LOOKUP_TIMEOUT, STREAM_CLIENT_CONNECT, $ctx);
                    if (!$socket) {
                        continue;
                    }
                    stream_set_blocking($socket, false);
                    stream_set_write_buffer($socket, 0);

                    //write to socket
                    $message = "GET /lookup?topic={$nsqLookupConf['topic']} HTTP/1.1\r\nHost: {$nsqLookupConf['host']}\r\n\r\n";
                    $msgLen = strlen($message);
                    if ($msgLen !== fwrite($socket, $message, $msgLen)) {
                        continue;
                    }

                    //read from socket
                    $chunkSize = 4096;
                    $r = [$socket];
                    $w = null;
                    $e = null;
                    $response = '';
                    while ((int)stream_select($r, $w, $e, self::LOOKUP_TIMEOUT)) {
                        $read = fread($socket, $chunkSize);
                        $response .= $read;
                        if (strlen($read) < $chunkSize) {
                            break;
                        }
                    }
                    $response = explode("\r\n\r\n", $response);
                    if (!($response[1] ?? null)) {
                        continue;
                    }
                    $response = json_decode($response[1], true);
                    if (JSON_ERROR_NONE !== json_last_error()) {
                        continue;
                    }
                    if (!($response['producers'] ?? null)) {
                        continue;
                    }
                    foreach ($response['producers'] as $idx => $producer) {
                        $tmpNsqdInstances[$producer['broadcast_address'] . ':' . $producer['tcp_port']] = $producer;
                    }

                    //close socket
                    if (is_resource($socket)) {
                        fclose($socket);
                    }
                }

                $lastLookup = time();
                $this->nsqdInstances = json_encode($tmpNsqdInstances);
            }
            sleep(1);
        }
    }
}