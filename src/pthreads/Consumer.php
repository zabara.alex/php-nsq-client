<?php
declare(strict_types=1);

namespace Libs\Nsq;

class Consumer extends \Thread
{
    private const FRAME_TYPE_RESPONSE = 0;
    private const FRAME_TYPE_ERROR = 1;
    private const FRAME_TYPE_MESSAGE = 2;

    private const FRAME_SIZE_BLOCK_SIZE = 4;
    private const FRAME_SIZE_BLOCK_TYPE = 4;

    private const LOCAL_FRAME_TYPE_FIN = 0;

    private const LOCAL_FRAME_SIZE_BLOCK_SIZE = 4;
    private const LOCAL_FRAME_SIZE_BLOCK_RID = 4;
    private const LOCAL_FRAME_SIZE_BLOCK_TYPE = 4;

    private const RESPONSE_HEARTBEAT = '_heartbeat_';
    private const TIMEOUT = 5;

    private const SELF_LOOKUP_INTERVAL = 45;

    private const MAX_READ_CHUNK_SIZE = 8192;

    private static $localSocket = null;

    /**
     * @var bool $stopSignal
     */
    public $stopSignal = false;

    /**
     * @var array|null $config
     */
    private $config = null;

    /**
     * @var Lookup $lookupThread
     */
    private $lookupThread = null;

    public function __construct(array $config)
    {
        if (!isset($config['lookup'], $config['clientId'], $config['hostname'], $config['topic'], $config['channel'])) {
            throw new \InvalidArgumentException('wrong params');
        }
        foreach ($config['lookup'] as $idx => $value) {
            $config['lookup'][$idx]['topic'] = $config['topic'];
        }
        $config['localSocketPath'] = 'unix:///tmp/nsqlib_' . getmypid(). '_' . md5(uniqid((string)microtime(), true));
        $this->config = $config;
    }

    public function __destruct()
    {
        is_resource(self::$localSocket) && fclose(self::$localSocket);
        $socketFilePath = str_replace('unix://', '', $this->config['localSocketPath']);
        file_exists($socketFilePath) && unlink($socketFilePath);
    }


    /**
     * wait for a message from main thread/context
     *
     * @param int $timeout
     * @return array|null
     */
    public function waitMessage(int $timeout) : ?array
    {
        $r = [self::$localSocket];
        $w = null;
        $e = null;
        $res = stream_select($r, $w, $e, $timeout);
        if (false === $res) {
            sleep($timeout);
            return null;
        }
        if (!$r) {
            return null;
        }
        $read = fread(self::$localSocket, self::LOCAL_FRAME_SIZE_BLOCK_SIZE);
        if ('' === $read){
            sleep($timeout);
            return null;
        }
        $frameSize = unpack('Nsize', $read)['size'] ?? null;
        if (!$frameSize) {
            return null;
        }
        $read = '';
        while (self::MAX_READ_CHUNK_SIZE < $frameSize) {
            $read .= fread(self::$localSocket, self::MAX_READ_CHUNK_SIZE);
            $frameSize -= self::MAX_READ_CHUNK_SIZE;
        }
        $read .= fread(self::$localSocket, $frameSize);
        if ('' === $read) {
            return null;
        }
        $frame = unpack('Nrid/a*data', $read);
        if (!isset($frame['rid'], $frame['data'])) {
            return null;
        }
        $msg = unpack('Jtimestamp/nattempts/a16id/a*payload', $frame['data']);
        if (!isset($msg['timestamp'], $msg['attempts'], $msg['id'], $msg['payload'])) {
            return null;
        }
        $msg['rid'] = $frame['rid'];
        return $msg;
    }

    /**
     * send FIN from main context/thread to local socket
     *
     * @param string $id - message id
     * @param int $rid - resource/socket id
     */
    public function ackMessage(string $id, int $rid) : void
    {
//        self::log('FIN.pre: ' . $id);
        $attempts = 0;
        $maxAttempts = 5;
        $message = pack('N', self::LOCAL_FRAME_TYPE_FIN) . pack('N', $rid) . $id . PHP_EOL;
        $msgLen = strlen($message) + self::LOCAL_FRAME_SIZE_BLOCK_SIZE;
        while ($msgLen !== fwrite(self::$localSocket, pack('N', $msgLen - self::LOCAL_FRAME_SIZE_BLOCK_SIZE) . $message, $msgLen) && ++$attempts < $maxAttempts) {
            self::log('fin.send.error attempts: ' . $attempts);
        }
        if ($maxAttempts <= $attempts) {
            self::log('fin.send.error.no.send');
        }
    }


    /**
     * connect main and new thread(contexts)
     */
    public function start($options = PTHREADS_INHERIT_ALL)
    {
        //run lookup thread
        $this->lookupThread = new Lookup($this->config['lookup']);
        $this->lookupThread->start();

        //create local socket in main thread
        self::$localSocket = stream_socket_server($this->config['localSocketPath'], $errno, $errstr, STREAM_SERVER_BIND|STREAM_SERVER_LISTEN);

        //connect to local socket from new thread
        $res = parent::start($options);

        //accept connection from main thread
        self::$localSocket = stream_socket_accept(self::$localSocket, 5);
        stream_set_blocking(self::$localSocket, false);
        stream_set_write_buffer(self::$localSocket, 0);

        return $res;
    }

    /**
     * new thread, new context
     */
    public function run() : void
    {
        $lookupNsqdInstancesStr = '';
        $currentNsqdInstances = [];
        $sockets = [];
        $lastLookupTime = 0;


        //create local socket for data sync
        $localSocket = stream_socket_client($this->config['localSocketPath'], $errno, $errstr, self::TIMEOUT, STREAM_CLIENT_CONNECT);
        stream_set_blocking($localSocket, false);
        stream_set_write_buffer($localSocket, 0);

        while (true) {

            //processing lookup
            if ($this->lookupThread->nsqdInstances &&
                ($this->lookupThread->nsqdInstances !== $lookupNsqdInstancesStr || $lastLookupTime + self::SELF_LOOKUP_INTERVAL < time())
            ) {
                $lastLookupTime = time();
                $lookupNsqdInstancesStr = $this->lookupThread->nsqdInstances;
                $lookupNsqdInstances = json_decode($lookupNsqdInstancesStr, true);
                $sockets = [];
                foreach ($lookupNsqdInstances as $idx => $nsqdInstance) {
                    $instanceKey = $nsqdInstance['broadcast_address'] . ':' . $nsqdInstance['tcp_port'];
                    if (is_resource($currentNsqdInstances[$instanceKey]['socket'] ?? null)) {
                        continue;
                    }

                    $socket = null;
                    $gateway = 'tcp://' . $nsqdInstance['broadcast_address'] . ':' . $nsqdInstance['tcp_port'];
                    $ctx = stream_context_create(['socket' => ['tcp_nodelay' => true]]);
                    $socket = stream_socket_client($gateway, $errno, $errstr, self::TIMEOUT, STREAM_CLIENT_CONNECT, $ctx);
                    if (!$socket) {
                        continue;
                    }
                    stream_set_blocking($socket, false);
                    stream_set_write_buffer($socket, 0);

                    //send magic identifier
                    if (4 !== fwrite($socket, '  V2', 4)) {
                        continue;
                    }

                    //send identity message
                    $identifyPayload = json_encode([
                        'client_id' => $this->config['clientId'],
                        'hostname' => $this->config['hostname'],
                        'feature_negotiation' => true,
                    ]);
                    $message = "IDENTIFY\n" . pack('N', strlen($identifyPayload)) . $identifyPayload;
                    $msgLen = strlen($message);
                    if ($msgLen !== fwrite($socket, $message, $msgLen)) {
                        continue;
                    }
                    //read identify response
                    $frame = self::readFrame($socket);
                    if (!$frame) {
                        continue;
                    }
                    if (self::FRAME_TYPE_RESPONSE !== $frame['type']) {
                        continue;
                    }
                    $response = json_decode($frame['data'] ?? '', true);
                    if (JSON_ERROR_NONE !== json_last_error()) {
                        continue;
                    }

                    //send sub message
                    $message = "SUB {$this->config['topic']} {$this->config['channel']} \n";
                    $msgLen = strlen($message);
                    if ($msgLen !== fwrite($socket, $message, $msgLen)) {
                        continue;
                    }
                    //read sub response
                    $frame = self::readFrame($socket);
                    if (!$frame) {
                        continue;
                    }
                    if (self::FRAME_TYPE_RESPONSE !== $frame['type']) {
                        continue;
                    }
                    if ('OK' !== $frame['data']) {
                        continue;
                    }

                    //send rdy message
                    $message = "RDY 1\n";
                    $msgLen = strlen($message);
                    if ($msgLen !== fwrite($socket, $message, $msgLen)) {
                        continue;
                    }

                    $currentNsqdInstances[$instanceKey]['socket'] = $socket;
                }

                foreach ($currentNsqdInstances as $idx => $nsqdInstance) {
                    if (!($lookupNsqdInstances[$idx] ?? null)) {
                        //close socket
                        if (is_resource($currentNsqdInstances[$idx]['socket'])) {
                            fclose($currentNsqdInstances[$idx]['socket']);
                        }
                        unset($currentNsqdInstances[$idx]);
                        continue;
                    }
                    $sockets[(int)$currentNsqdInstances[$idx]['socket']] = $currentNsqdInstances[$idx]['socket'];
                }
            } //end processing lookup



            //waiting messages

            if (!$sockets) {
                self::log('no.sockets.to.listening');
                if ($this->stopSignal) {
                    $this->lookupThread->stopSignal = true;
                    break;
                }
                sleep(self::TIMEOUT);
                continue;
            }

            $r = $sockets;
            $r[] = $localSocket;
            $w = null;
            $e = null;

            stream_select($r, $w, $e, self::TIMEOUT);

            if (!$r) {
                self::log('stream.select.timeout');
                if ($this->stopSignal) {
                    $this->lookupThread->stopSignal = true;
                    break;
                }
                continue;
            }

            //processing messages from local(main) thread first
            if (in_array($localSocket, $r, true)) {
                $read = fread($localSocket, self::LOCAL_FRAME_SIZE_BLOCK_SIZE);
                $frameSize = unpack('N', $read)[1] ?? null;
                if (!$frameSize) {
                    continue;
                }
                $read = fread($localSocket, self::LOCAL_FRAME_SIZE_BLOCK_TYPE);
                $frameType = unpack('N', $read)[1] ?? null;
                if (null === $frameType) {
                    continue;
                }

                if (self::LOCAL_FRAME_TYPE_FIN === $frameType) {
                    $read = fread($localSocket, $frameSize - self::LOCAL_FRAME_SIZE_BLOCK_TYPE);
                    $frame = unpack('Nrid/a16id', $read);
                    if (!isset($frame['rid'], $frame['id'])) {
                        continue;
                    }
                    //FIN
                    if (!isset($sockets[$frame['rid']])) {
                        continue;
                    }
                    $message = "FIN {$frame['id']} \n";
                    $msgLen = strlen($message);
                    if ($msgLen !== fwrite($sockets[$frame['rid']], $message, $msgLen)) {
//                            continue;
                    }
//                    self::log('FIN.post: ' . $frame['id']);
                }

                continue;
            } //end processing local socket

            //gracefully stop thread
            if ($this->stopSignal) {
                $this->lookupThread->stopSignal = true;
                break;
            }

            //processing messages from nsqd instances
            $curSocket = $r[array_rand($r)];

            //read frame size
            $read = fread($curSocket, self::FRAME_SIZE_BLOCK_SIZE);
            if ('' === $read) {
                //connection closed by remote host
                fclose($curSocket);
                unset($sockets[(int)$curSocket]);
                self::log('connection.closed.by.remote.host');
                continue;
            }

            $frameSize = unpack('N', $read)[1] ?? null;
            if (!$frameSize) {
                self::log('frame.size.error');
                continue;
            }

//            self::log('frame.size: ' . $frameSize);
            $read = '';
            $readAttempts = 0;
            $needReadBytes = $frameSize;
            while (0 < $needReadBytes && $readAttempts < 5) {
                $chunkSize = self::MAX_READ_CHUNK_SIZE;
                $needReadBytes < self::MAX_READ_CHUNK_SIZE && $chunkSize = $needReadBytes;
                $readChunk = fread($curSocket, $chunkSize);
                if (!$readChunk) {
                    self::log('read.chunk.empty');
                    $readAttempts++;
                    usleep(50000);
                    continue;
                }
//                self::log('chunk.size.while: ' . $chunkSize);
                $readAttempts = 0;
                $needReadBytes -= mb_strlen($readChunk, '8bit');
//                self::log('read.chunk: ' . $readChunk);
                $read .= $readChunk;
            }
            if (5 <= $readAttempts) {
                self::log('read.attempts.break');
            }

            $frame = unpack('Ntype/a*data', $read);

            if (!isset($frame['type'], $frame['data'])) {
                self::log('frame.format.error');
                continue;
            }

            switch (true){
                case self::FRAME_TYPE_RESPONSE === $frame['type']:
                    if (self::RESPONSE_HEARTBEAT !== $frame['data']) {
                        self::log('frame.response.message.unknown');
                        break;
                    }
                    //heartbeat
                    if (4 !== fwrite($curSocket, "NOP\n", 4)) {
                        self::log('heartbeat.response.error');
                    }
                    self::log('heartbeat.success');
                    break;
                case self::FRAME_TYPE_MESSAGE === $frame['type']:
//                    self::log('frame.type.message');
                    //process message
                    $msg = unpack('Jtimestamp/nattempts/a16id/a*payload', $frame['data']);

                    $lw = [$localSocket];
                    $lr = $le = null;
                    stream_select($lr, $lw, $le, 0);
                    if (!$lw) {
                        //REQ
                        $message = "REQ {$msg['id']} " . mt_rand(1, 3) . "\n";
                        $msgLen = strlen($message);
                        if ($msgLen !== fwrite($curSocket, $message, $msgLen)) {
//                            continue;
                        }
                        self::log('local.socket.unavailable.send.req.to.nsqd');
                        break;
                    }

                    $msgLen = $frameSize - self::FRAME_SIZE_BLOCK_TYPE + self::LOCAL_FRAME_SIZE_BLOCK_RID;
//                    self::log('msg.len: ' . $msgLen);
                    $bytesWritten = fwrite($localSocket, pack('N', $msgLen) . pack('N', (int)$curSocket) . $frame['data'], $msgLen + self::LOCAL_FRAME_SIZE_BLOCK_SIZE);
//                    self::log('bytes.written: ' . (int)$bytesWritten);
                    break;
                case self::FRAME_TYPE_ERROR === $frame['type']:
                    self::log('nsq.protocol.frame.type.error ' . $frame['data']);
                    break;
                default:
                    self::log('nsq.protocol.wrong.frame.type ' . $frame['type']);
                    break;
            }
        }

        is_resource($localSocket) && fclose($localSocket);
        foreach ($sockets as &$socket) {
            is_resource($socket) && fclose($socket);
        }
    }

    public function getLookupThread() : ?Lookup
    {
        return $this->lookupThread;
    }

    private static function readFrame(&$socket) : ?array
    {
        $r = [$socket];
        $w = null;
        $e = null;
        stream_select($r, $w, $e, self::TIMEOUT);
        if (!$r) {
            self::log('read.frame.stream.select.empty.sockets');
            return null;
        }
        $read = fread($socket, self::FRAME_SIZE_BLOCK_SIZE);
        $frameSize = unpack('N', $read)[1] ?? null;
        if (!$frameSize) {
            self::log('frame.size.error');
            return null;
        }
        $read = fread($socket, $frameSize);
        if (false === $read) {
            self::log('frame.read.error');
            return null;
        }
        $frame = unpack('Ntype/a*data', $read);
        if (!isset($frame['type'], $frame['data'])) {
            self::log('frame.format.error');
            return null;
        }
        return $frame;
    }

    private static function log($message) : void
    {
        echo '[' . date('Y-m-d H:i:s') . '] nsqlib.DEBUG: ' . $message . PHP_EOL;
    }
}
