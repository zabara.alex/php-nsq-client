<?php
declare(strict_types=1);

namespace Nsq;

class Consumer
{
    private const CHANNEL_NAME_STOP_LOOKUP = 'csl';
    private const CHANNEL_NAME_STOP_CONSUMER = 'csc';
    private const CHANNEL_NAME_LOOKUP = 'cl';
    private const CHANNEL_NAME_MSG = 'cm';
    private const CHANNEL_NAME_ACK = 'ca';

    private const THREAD_NAME_LOOKUP = 'l';
    private const THREAD_NAME_CONSUMER = 'c';

    private const FRAME_TYPE_RESPONSE = 0;
    private const FRAME_TYPE_ERROR = 1;
    private const FRAME_TYPE_MESSAGE = 2;

    private const FRAME_SIZE_BLOCK_SIZE = 4;
    private const FRAME_SIZE_BLOCK_TYPE = 4;

    private const RESPONSE_HEARTBEAT = '_heartbeat_';
    private const TIMEOUT_STREAM_CREATE = 5;
    private const TIMEOUT_STREAM_SELECT = 1;

    private const LOOKUP_INTERVAL = 45;

    private const MAX_READ_CHUNK_SIZE = 8192;

    private const LOCAL_SOCK_MSG = '1';
    private const LOCAL_SOCK_MSG_SIZE = 1;

    private array $channels = [];
    private array $threads = [];
    private array $events = [];

    private $localSocket = null;

    /**
     * @var array|null $config
     */
    private array $config = [];

    public function __construct(array $config)
    {
        if (!isset($config['lookupInstances'], $config['clientId'], $config['hostname'], $config['topic'], $config['channel'])) {
            throw new \InvalidArgumentException('wrong params');
        }
        $this->config = $config;
    }

    public function __destruct()
    {
        if (is_resource($this->localSocket)) {
            stream_socket_shutdown($this->localSocket, \STREAM_SHUT_RDWR);
            fclose($this->localSocket);
        }
    }

    /**
     * start consumer
     * run consumer and lookup threads
     */
    public function start() : bool
    {
        //create local socket in main thread
        $this->localSocket = stream_socket_server('tcp://127.0.0.1:0', $errno, $errstr, STREAM_SERVER_BIND|STREAM_SERVER_LISTEN);
        if (!$this->localSocket) {
            self::log('local.socket.creation.error');
            return false;
        }
        $locSockName = stream_socket_get_name($this->localSocket, false);
        $locSockPort = (int)substr($locSockName, strpos($locSockName, ':') + 1);

        $this->channels[self::CHANNEL_NAME_STOP_LOOKUP] = \parallel\Channel::make(self::CHANNEL_NAME_STOP_LOOKUP, $capacity = 1);
        $this->channels[self::CHANNEL_NAME_STOP_CONSUMER] = \parallel\Channel::make(self::CHANNEL_NAME_STOP_CONSUMER, $capacity = 1);
        $this->channels[self::CHANNEL_NAME_LOOKUP] = \parallel\Channel::make(self::CHANNEL_NAME_LOOKUP, $capacity = \parallel\Channel::Infinite);
        $this->channels[self::CHANNEL_NAME_MSG] = \parallel\Channel::make(self::CHANNEL_NAME_MSG, $capacity = \parallel\Channel::Infinite);
        $this->channels[self::CHANNEL_NAME_ACK] = \parallel\Channel::make(self::CHANNEL_NAME_ACK, $capacity = \parallel\Channel::Infinite);

        $this->events[self::CHANNEL_NAME_MSG] = new \parallel\Events();
        $this->events[self::CHANNEL_NAME_MSG]->addChannel($this->channels[self::CHANNEL_NAME_MSG]);
        $this->events[self::CHANNEL_NAME_MSG]->setBlocking(false);

        $lookupChannels = [
            self::CHANNEL_NAME_STOP_LOOKUP => $this->channels[self::CHANNEL_NAME_STOP_LOOKUP],
            self::CHANNEL_NAME_LOOKUP => $this->channels[self::CHANNEL_NAME_LOOKUP],
        ];
        $this->threads[self::THREAD_NAME_LOOKUP] = new \parallel\Runtime(__FILE__);
        $this->threads[self::THREAD_NAME_LOOKUP]->run(self::lookupFn(), [$lookupChannels, $this->config['lookupInstances'], $this->config['topic']]);

        $consumerParams = [
            'config' => $this->config,
            'localSocketPort' => $locSockPort,
        ];
        $consumerChannels = [
            self::CHANNEL_NAME_STOP_CONSUMER => $this->channels[self::CHANNEL_NAME_STOP_CONSUMER],
            self::CHANNEL_NAME_MSG => $this->channels[self::CHANNEL_NAME_MSG],
            self::CHANNEL_NAME_LOOKUP => $this->channels[self::CHANNEL_NAME_LOOKUP],
            self::CHANNEL_NAME_ACK => $this->channels[self::CHANNEL_NAME_ACK],
        ];
        $this->threads[self::THREAD_NAME_CONSUMER] = new \parallel\Runtime(__FILE__);
        $this->threads[self::THREAD_NAME_CONSUMER]->run(self::consumerFn(), [$consumerParams, $consumerChannels]);

        //accept connection from main thread
        $this->localSocket = stream_socket_accept($this->localSocket, self::TIMEOUT_STREAM_CREATE);
        if (!$this->localSocket) {
            self::log('local.socket.accept.error');
            $this->stop();
            return false;
        }
        stream_set_blocking($this->localSocket, false);
        stream_set_write_buffer($this->localSocket, 0);

        return true;
    }

    /**
     * stop consumer, kill all threads
     */
    public function stop() : void
    {
        self::log('lookup.thread.waiting...');
        $this->channels[self::CHANNEL_NAME_STOP_LOOKUP]->close();
        $this->threads[self::THREAD_NAME_LOOKUP]->close();
        self::log('lookup.thread.stopped');

        self::log('consumer.thread.waiting...');
        $this->channels[self::CHANNEL_NAME_STOP_CONSUMER]->close();
        $this->threads[self::THREAD_NAME_CONSUMER]->close();
        self::log('consumer.thread.stopped');

        if (is_resource($this->localSocket)) {
            stream_socket_shutdown($this->localSocket, \STREAM_SHUT_RDWR);
            fclose($this->localSocket);
        }
    }

    /**
     * wait for a message from main thread/context
     *
     * @param int $timeout - microseconds
     * @return array|null
     */
    public function waitMessage(int $timeout) : ?array
    {
        $event = $this->events[self::CHANNEL_NAME_MSG]->poll();
        if ($event instanceof \parallel\Events\Event && \parallel\Events\Event\Type::Read === $event->type) {
            $this->events[self::CHANNEL_NAME_MSG]->addChannel($this->channels[self::CHANNEL_NAME_MSG]);
            return $event->value;
        }

        $r = [$this->localSocket];
        $w = null;
        $e = null;
        stream_select($r, $w, $e, 0, $timeout);
//        set_error_handler(static function($errno, $errstr) { throw new \RuntimeException($errstr);});
//        try {
//            stream_select($r, $w, $e, 0, $timeout);
//        } catch (\Throwable $e) {
//            !strpos($e->getMessage(), 'max_fd') && self::log('wait.message.stream.select.exception: ' .$e->getMessage());
//        }
//        restore_error_handler();
        if (!$r) {
            return null;
        }
        fread($this->localSocket, self::LOCAL_SOCK_MSG_SIZE);
        $event = $this->events[self::CHANNEL_NAME_MSG]->poll();
        if ($event instanceof \parallel\Events\Event && \parallel\Events\Event\Type::Read === $event->type) {
            $this->events[self::CHANNEL_NAME_MSG]->addChannel($this->channels[self::CHANNEL_NAME_MSG]);
            return $event->value;
        }
        return null;
    }

    /**
     * send ack from main context/thread to ack channel
     *
     * @param string $id - message id
     * @param int $rid - resource/socket id
     */
    public function ackMessage(string $id, int $rid) : void
    {
        $this->channels[self::CHANNEL_NAME_ACK]->send([
            'id' => $id,
            'rid' => $rid,
        ]);
        fwrite($this->localSocket, self::LOCAL_SOCK_MSG);
    }

    /**
     * consumer thread code
     * @return \Closure
     */
    private static function consumerFn() : \Closure
    {
        return static function (array $params, array $channels) : void {
            $params += [
                'config' => [],
                'localSocketPort' => null,
            ];

            //create local socket for data sync
            $localSocket = stream_socket_client("tcp://127.0.0.1:{$params['localSocketPort']}", $errno, $errstr, self::TIMEOUT_STREAM_CREATE, STREAM_CLIENT_CONNECT);
            stream_set_blocking($localSocket, false);
            stream_set_write_buffer($localSocket, 0);

            $channels += [
                self::CHANNEL_NAME_STOP_CONSUMER => null,
                self::CHANNEL_NAME_LOOKUP => null,
                self::CHANNEL_NAME_MSG => null,
                self::CHANNEL_NAME_ACK => null,
            ];

            $eventsStop = new \parallel\Events();
            $eventsStop->addChannel($channels[self::CHANNEL_NAME_STOP_CONSUMER]);
            $eventsStop->setBlocking(false);

            $eventsLookup = new \parallel\Events();
            $eventsLookup->addChannel($channels[self::CHANNEL_NAME_LOOKUP]);
            $eventsLookup->setBlocking(false);

            $eventsAck = new \parallel\Events();
            $eventsAck->addChannel($channels[self::CHANNEL_NAME_ACK]);
            $eventsAck->setBlocking(false);

            $lookupNsqdInstances = [];
            $currentNsqdInstances = [];
            $sockets = [];

            while (true) {
                //processing stop signal
                if ($eventsStop->poll()) {
                    self::log('consumer.stop.channel.closed');
                    self::processAck($sockets, $channels, $eventsAck);
                    foreach ($sockets as &$socket) {
                        stream_socket_shutdown($socket, \STREAM_SHUT_RDWR);
                        is_resource($socket) && fclose($socket);
                    }
                    stream_socket_shutdown($localSocket, \STREAM_SHUT_RDWR);
                    is_resource($localSocket) && fclose($localSocket);
                    break;
                }

                //processing lookup
                self::processLookup($sockets, $channels, $lookupNsqdInstances, $currentNsqdInstances, $params, $eventsLookup);
                //end processing lookup

                if (!$sockets) {
                    self::log('consumer.no.sockets.to.listening');
                    usleep(1000000);
                    continue;
                }

                //ack messages
                self::processAck($sockets, $channels, $eventsAck);
                //end ack messages

                //waiting for new messages
                $r = $sockets;
                $r[] = $localSocket;
                $w = null;
                $e = null;
                stream_select($r, $w, $e, 0, 100000);
                if (!$r) {
                    continue;
                }
                //processing ack messages from local(main) thread first
                foreach ($r as $k => $_) {
                    if ($localSocket === $_) {
                        fread($localSocket, self::LOCAL_SOCK_MSG_SIZE);
                        self::processAck($sockets, $channels, $eventsAck);
                        unset($r[$k]);
                        break;
                    }
                }
                if (!$r) {
                    continue;
                }
                //end processing local socket

                //processing messages from nsqd instances
                $curSocket = $r[array_rand($r)];
                //read frame size
                $read = fread($curSocket, self::FRAME_SIZE_BLOCK_SIZE);
                if ('' === $read) {
                    //connection closed by remote host
                    fclose($curSocket);
                    unset($sockets[(int)$curSocket]);
                    self::log('consumer.connection.closed.by.remote.host');
                    continue;
                }

                $frameSize = unpack('N', $read)[1] ?? null;
                if (!$frameSize) {
                    self::log('consumer.frame.size.error');
                    continue;
                }

                $read = '';
                $readAttempts = 0;
                $needReadBytes = $frameSize;
                while (0 < $needReadBytes && $readAttempts < 5) {
                    $chunkSize = self::MAX_READ_CHUNK_SIZE;
                    $needReadBytes < self::MAX_READ_CHUNK_SIZE && $chunkSize = $needReadBytes;
                    $readChunk = fread($curSocket, $chunkSize);
                    if (!$readChunk) {
                        self::log('consumer.read.chunk.empty');
                        $readAttempts++;
                        usleep(50000);
                        continue;
                    }
                    $readAttempts = 0;
                    $needReadBytes -= mb_strlen($readChunk, '8bit');
                    $read .= $readChunk;
                }
                if (5 <= $readAttempts) {
                    self::log('consumer.read.attempts.break');
                }

                $frame = unpack('Ntype/a*data', $read);
                if (!isset($frame['type'], $frame['data'])) {
                    self::log('consumer.frame.format.error');
                    continue;
                }

                switch (true) {
                    case self::FRAME_TYPE_RESPONSE === $frame['type']:
                        if (self::RESPONSE_HEARTBEAT !== $frame['data']) {
                            self::log('consumer.frame.response.message.unknown');
                            break;
                        }
                        //heartbeat
                        if (4 !== fwrite($curSocket, "NOP\n", 4)) {
                            self::log('consumer.heartbeat.response.error');
                        }
                        self::log('consumer.heartbeat.success: sock.name : ' . stream_socket_get_name($curSocket, true));
                        break;
                    case self::FRAME_TYPE_MESSAGE === $frame['type']:
                        //process message
                        $msg = unpack('Jtimestamp/nattempts/a16id/a*payload', $frame['data']);
                        $msg['rid'] = (int)$curSocket;
                        try {
                            $channels[self::CHANNEL_NAME_MSG]->send($msg);
                            fwrite($localSocket, self::LOCAL_SOCK_MSG);
                        } catch (\Throwable $e) {
                            //REQ
                            $message = "REQ {$msg['id']} " . \random_int(1, 3) . "\n";
                            $msgLen = strlen($message);
                            if ($msgLen !== fwrite($curSocket, $message, $msgLen)) {
                                self::log('consumer.channel.send.error.send.req.to.nsqd');
                            }
                            break;
                        }
                        break;
                    case self::FRAME_TYPE_ERROR === $frame['type']:
                        self::log('consumer.nsq.protocol.frame.type.error ' . $frame['data']);
                        break;
                    default:
                        self::log('consumer.nsq.protocol.wrong.frame.type ' . $frame['type']);
                        break;
                }
            }
        };
    }

    /**
     * process acks for consumer thread
     * @param array $sockets
     * @param array $channels
     * @param \parallel\Events $eventsAck
     */
    private static function processAck(array &$sockets, array &$channels, \parallel\Events $eventsAck) : void
    {
        while ($event = $eventsAck->poll()) {
            if (! $event instanceof \parallel\Events\Event) {
                throw new \RuntimeException('ack.message.unexpected.event');
            }
            if ($channels[self::CHANNEL_NAME_ACK] === $event->object && \parallel\Events\Event\Type::Read === $event->type) {
                $eventsAck->addChannel($channels[self::CHANNEL_NAME_ACK]);
                if (!isset($event->value['id'], $event->value['rid'])) {
                    self::log('consumer.ack.id.rid.empty');
                    continue;
                }
                if (!isset($sockets[$event->value['rid']])) {
                    self::log('consumer.ack.no.socket');
                    continue;
                }
                $message = "FIN {$event->value['id']} \n";
                $msgLen = strlen($message);
                if ($msgLen !== fwrite($sockets[$event->value['rid']], $message, $msgLen)) {
                    self::log('consumer.ack.write.to.socket.error');
                    continue;
                }
                return;
            }
            throw new \RuntimeException('wait.message.unexpected.event.type');
        }
    }

    /**
     * process lookup for consumer thread
     * @param array $sockets
     * @param array $channels
     * @param array $lookupNsqdInstances
     * @param array $currentNsqdInstances
     * @param array $params
     * @param \parallel\Events $eventsLookup
     */
    private static function processLookup(array &$sockets, array &$channels, array &$lookupNsqdInstances, array &$currentNsqdInstances, array &$params, \parallel\Events $eventsLookup) : void
    {
        $event = null;
        while ($_ = $eventsLookup->poll()) {
            $eventsLookup->addChannel($channels[self::CHANNEL_NAME_LOOKUP]);
            $event = $_;
            if ($event instanceof \parallel\Events\Event && $channels[self::CHANNEL_NAME_LOOKUP] === $event->object && \parallel\Events\Event\Type::Close === $event->type) {
                break;
            }
        }
        if ($event instanceof \parallel\Events\Event && $channels[self::CHANNEL_NAME_LOOKUP] === $event->object && \parallel\Events\Event\Type::Read === $event->type) {
            if ($event->value !== $lookupNsqdInstances) {
                self::log('consumer.lookup.processing');
                $lookupNsqdInstances = $event->value;
                $sockets = [];
                foreach ($lookupNsqdInstances as $idx => $nsqdInstance) {
                    $instanceKey = $nsqdInstance['broadcast_address'] . ':' . $nsqdInstance['tcp_port'];
                    if (is_resource($currentNsqdInstances[$instanceKey]['socket'] ?? null)) {
                        continue;
                    }
                    $socket = null;
                    $gateway = 'tcp://' . $nsqdInstance['broadcast_address'] . ':' . $nsqdInstance['tcp_port'];
                    $ctx = stream_context_create(['socket' => ['tcp_nodelay' => true]]);
                    $socket = stream_socket_client($gateway, $errno, $errstr, self::TIMEOUT_STREAM_CREATE, STREAM_CLIENT_CONNECT, $ctx);
                    if (!$socket) {
                        continue;
                    }
                    stream_set_blocking($socket, false);
                    stream_set_write_buffer($socket, 0);

                    //send magic identifier
                    if (4 !== fwrite($socket, '  V2', 4)) {
                        continue;
                    }

                    //send identity message
                    $identifyPayload = json_encode([
                        'client_id' => $params['config']['clientId'],
                        'hostname' => $params['config']['hostname'],
                        'feature_negotiation' => true,
                    ]);
                    $message = "IDENTIFY\n" . pack('N', strlen($identifyPayload)) . $identifyPayload;
                    $msgLen = strlen($message);
                    if ($msgLen !== fwrite($socket, $message, $msgLen)) {
                        continue;
                    }
                    //read identify response
                    $frame = self::readFrame($socket);
                    if (!$frame) {
                        continue;
                    }
                    if (self::FRAME_TYPE_RESPONSE !== $frame['type']) {
                        continue;
                    }
                    $response = json_decode($frame['data'] ?? '', true);
                    if (JSON_ERROR_NONE !== json_last_error()) {
                        continue;
                    }

                    //send sub message
                    $message = "SUB {$params['config']['topic']} {$params['config']['channel']} \n";
                    $msgLen = strlen($message);
                    if ($msgLen !== fwrite($socket, $message, $msgLen)) {
                        continue;
                    }
                    //read sub response
                    $frame = self::readFrame($socket);
                    if (!$frame) {
                        continue;
                    }
                    if (self::FRAME_TYPE_RESPONSE !== $frame['type']) {
                        continue;
                    }
                    if ('OK' !== $frame['data']) {
                        continue;
                    }

                    //send rdy message
                    $message = "RDY 1\n";
                    $msgLen = strlen($message);
                    if ($msgLen !== fwrite($socket, $message, $msgLen)) {
                        continue;
                    }

                    $currentNsqdInstances[$instanceKey]['socket'] = $socket;
                }

                foreach ($currentNsqdInstances as $idx => $nsqdInstance) {
                    if (!($lookupNsqdInstances[$idx] ?? null)) {
                        //close socket
                        if (is_resource($currentNsqdInstances[$idx]['socket'])) {
                            fclose($currentNsqdInstances[$idx]['socket']);
                        }
                        unset($currentNsqdInstances[$idx]);
                        continue;
                    }
                    $sockets[(int)$currentNsqdInstances[$idx]['socket']] = $currentNsqdInstances[$idx]['socket'];
                }
            }
        }
    }

    /**
     * lookup thread code
     * @return \Closure
     */
    private static function lookupFn() : \Closure
    {
        return static function (array $channels, array $lookupInstances, string $topic) : void {
            $channels += [
                self::CHANNEL_NAME_STOP_LOOKUP => null,
                self::CHANNEL_NAME_LOOKUP => null,
            ];

            $eventsStop = new \parallel\Events();
            $eventsStop->addChannel($channels[self::CHANNEL_NAME_STOP_LOOKUP]);
            $eventsStop->setBlocking(false);

            $lastLookup = 0;
            $nsqdInstances = [];

            while (true) {
                usleep(500000);
                //processing stop signal
                if ($eventsStop->poll()) {
                    self::log('lookup.stop.channel.closed');
                    break;
                }
                if (!$nsqdInstances || $lastLookup + self::LOOKUP_INTERVAL < time()) {
                    $nsqdInstances = [];
                    foreach ($lookupInstances as $nsqLookupConf) {
                        $gateway = 'tcp://' . $nsqLookupConf['host'] . ':' . $nsqLookupConf['port'];
                        $ctx = stream_context_create(['socket' => ['tcp_nodelay' => true]]);
                        $socket = stream_socket_client($gateway, $errno, $errstr, self::TIMEOUT_STREAM_CREATE, STREAM_CLIENT_CONNECT, $ctx);
                        if (!$socket) {
                            continue;
                        }
                        stream_set_blocking($socket, false);
                        stream_set_write_buffer($socket, 0);

                        //write to socket
                        $message = "GET /lookup?topic={$topic} HTTP/1.1\r\nHost: {$nsqLookupConf['host']}\r\n\r\n";
                        $msgLen = strlen($message);
                        if ($msgLen !== fwrite($socket, $message, $msgLen)) {
                            self::log('lookup.socket.write.error');
                            continue;
                        }

                        //read from socket
                        $chunkSize = 4096;
                        $r = [$socket];
                        $w = $e = null;
                        $response = '';
                        while ((int)stream_select($r, $w, $e, self::TIMEOUT_STREAM_SELECT)) {
                            $read = fread($socket, $chunkSize);
                            $response .= $read;
                            if (strlen($read) < $chunkSize) {
                                break;
                            }
                        }
                        $response = explode("\r\n\r\n", $response);
                        if (!($response[1] ?? null)) {
                            self::log('lookup.response.error');
                            continue;
                        }
                        try {
                            $response = json_decode($response[1], true, 512, \JSON_THROW_ON_ERROR);
                        } catch (\JsonException $e) {
                            self::log('lookup.json.invalid');
                            continue;
                        }
                        if (!($response['producers'] ?? null)) {
//                            self::log('lookup.producers.error');
                            continue;
                        }
                        foreach ($response['producers'] as $idx => $producer) {
                            $nsqdInstances[$producer['broadcast_address'] . ':' . $producer['tcp_port']] = $producer;
                        }
                        //close socket
                        is_resource($socket) && fclose($socket);
                    }
                    $lastLookup = time();
                    $channels[self::CHANNEL_NAME_LOOKUP]->send($nsqdInstances);
                }
            }
        };
    }

    private static function readFrame(&$socket) : ?array
    {
        $r = [$socket];
        $w = null;
        $e = null;
        stream_select($r, $w, $e, self::TIMEOUT_STREAM_SELECT);
        if (!$r) {
            self::log('read.frame.stream.select.empty.sockets');
            return null;
        }
        $read = fread($socket, self::FRAME_SIZE_BLOCK_SIZE);
        $frameSize = unpack('N', $read)[1] ?? null;
        if (!$frameSize) {
            self::log('read.frame.size.error');
            return null;
        }
        $read = fread($socket, $frameSize);
        if (false === $read) {
            self::log('read.frame.fread.error');
            return null;
        }
        $frame = unpack('Ntype/a*data', $read);
        if (!isset($frame['type'], $frame['data'])) {
            self::log('read.frame.format.error');
            return null;
        }
        return $frame;
    }

    private static function log($message) : void
    {
        echo '[' . date('Y-m-d H:i:s') . '] nsqlib.DEBUG: ' . $message . PHP_EOL;
    }
}
