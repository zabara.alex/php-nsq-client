<?php
declare(strict_types=1);

namespace Nsq;

class Publisher
{
    private const FRAME_TYPE_RESPONSE = 0;
    private const FRAME_TYPE_ERROR = 1;
    private const FRAME_TYPE_MESSAGE = 2;

    private const FRAME_SIZE_BLOCK_SIZE = 4;
    private const FRAME_SIZE_BLOCK_TYPE = 4;

    private const TIMEOUT = 2;

    private static $socket = null;

    private $config = null;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function send(string $task, string $message) : bool
    {
        if (!is_resource(self::$socket)) {
            $gateway = 'tcp://' . $this->config['nsqd'];
            $ctx = stream_context_create(['socket' => ['tcp_nodelay' => true]]);
            self::$socket = stream_socket_client($gateway, $errno, $errstr, self::TIMEOUT, STREAM_CLIENT_CONNECT, $ctx);
            if (!self::$socket) {
                return false;
            }
            //send magic identifier
            if (4 !== fwrite(self::$socket, '  V2', 4)) {
                return false;
            }
        }

        //send pub message
        $msgLen = strlen($message);
        $message = "PUB {$task}\n" . pack('N', $msgLen) . $message;
        $msgLen = strlen($message);
        if ($msgLen !== fwrite(self::$socket, $message, $msgLen)) {
            return false;
        }
        //read sub response
        $frame = self::readFrame(self::$socket);
        if (!$frame) {
            return false;
        }
        if (self::FRAME_TYPE_RESPONSE !== $frame['type']) {
            return false;
        }
        if ('OK' !== $frame['data']) {
            return false;
        }
        return true;
    }

    public function disconnect() : void
    {
        is_resource(self::$socket) && fclose(self::$socket);
        self::$socket = null;
    }

    public function __destruct()
    {
        is_resource(self::$socket) && fclose(self::$socket);
    }

    private static function readFrame(&$socket) : ?array
    {
        $r = [$socket];
        $w = null;
        $e = null;
        stream_select($r, $w, $e, self::TIMEOUT);
        if (!$r) {
            self::log('read.frame.stream.select.empty.sockets');
            return null;
        }
        $read = fread($socket, self::FRAME_SIZE_BLOCK_SIZE);
        $frameSize = unpack('N', $read)[1] ?? null;
        if (!$frameSize) {
            self::log('frame.size.error');
            return null;
        }
        $read = fread($socket, $frameSize);
        if (false === $read) {
            self::log('frame.read.error');
            return null;
        }
        $frame = unpack('Ntype/a*data', $read);
        if (!isset($frame['type'], $frame['data'])) {
            self::log('frame.format.error');
            return null;
        }
        return $frame;
    }

    private static function log($message) : void
    {
        return;
        echo '[' . date('Y-m-d H:i:s') . '] nsqlib.DEBUG: ' . $message . PHP_EOL;
    }
}